# CodeChallenge

This is a demo project to a microservice to expose a REST server to create, update, delete or consult one or all users.




## Run
To run you need to open a terminal (**Mac & Linux**) or cmd prompt (**Windows**)  and go to the base project folder and execute:

**Mac & Linux** execute ./mvnw 
**Windows** execute mvnw

This start an embedded Tomcat server:
**host:** localhost
**port:** 8081
**project path:** CodeChallenge
**Explorer view** http://localhost:8081/CodeChallenge

## Database

When the project run, start a H2 database to persist data, you can view changes on
http://localhost:8081/CodeChallenge/h2-console

## Consume service
To consume service the api is hosted on next url
http://localhost:8081/CodeChallenge/api
| Action |Http Method  |Api|Description
|--|--|--|--
| Get a user by ID | GET |/users/{id}|Get one user by the provided id 
| Create new user |POST  |/users|Create one user with the json sended, by default the user is persist on ACTIVE status, don't need to send that value on json file
| Update existing user | PUT |/users|Updates the json data sended
| Delete user | DELETE |/users/{id}|Put status value on INACTIVE the provided user id
| Get all users| GET |/users|Get all ACTIVE status users on database

> You can test with provided postmant_environment and postman_collection included on the base project folder.

## Unit test
You can test the service implementation class, need build and run the **/src/test/java/com/fergarcia/codechallenge/web/rest/UserxResourceIntTest.java** on base project folder



