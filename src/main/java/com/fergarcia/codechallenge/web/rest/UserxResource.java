package com.fergarcia.codechallenge.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fergarcia.codechallenge.domain.Userx;
import com.fergarcia.codechallenge.service.UserxService;
import com.fergarcia.codechallenge.web.rest.errors.BadRequestAlertException;
import com.fergarcia.codechallenge.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Userx.
 */
@RestController
@RequestMapping("/api")
public class UserxResource {

    private final Logger log = LoggerFactory.getLogger(UserxResource.class);

    private static final String ENTITY_NAME = "codeChallengeUserx";

    private final UserxService userxService;

    public UserxResource(UserxService userxService) {
        this.userxService = userxService;
    }

    /**
     * POST  /userxes : Create a new userx.
     *
     * @param userx the userx to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userx, or with status 400 (Bad Request) if the userx has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/users")
    @Timed
    public ResponseEntity<Userx> createUserx(@RequestBody Userx userx) throws URISyntaxException {
        log.debug("REST request to save Userx : {}", userx);
        if (userx.getId() != null) {
            throw new BadRequestAlertException("A new userx cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Userx result = userxService.save(userx);
        return ResponseEntity.created(new URI("/api/userxes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /userxes : Updates an existing userx.
     *
     * @param userx the userx to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userx,
     * or with status 400 (Bad Request) if the userx is not valid,
     * or with status 500 (Internal Server Error) if the userx couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/users")
    @Timed
    public ResponseEntity<Userx> updateUserx(@RequestBody Userx userx) throws URISyntaxException {
        log.debug("REST request to update Userx : {}", userx);
        if (userx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Userx result = userxService.save(userx);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userx.getId().toString()))
            .body(result);
    }

    /**
     * GET  /userxes : get all the userxes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userxes in body
     */
    @GetMapping("/users")
    @Timed
    public List<Userx> getAllUserxes() {
        log.debug("REST request to get all Userxes");
        return userxService.findAll();
    }

    /**
     * GET  /userxes/:id : get the "id" userx.
     *
     * @param id the id of the userx to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userx, or with status 404 (Not Found)
     */
    @GetMapping("/users/{id}")
    @Timed
    public ResponseEntity<Userx> getUserx(@PathVariable Long id) {
        log.debug("REST request to get Userx : {}", id);
        Optional<Userx> userx = userxService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userx);
    }

    /**
     * DELETE  /userxes/:id : delete the "id" userx.
     *
     * @param id the id of the userx to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/users/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserx(@PathVariable Long id) {
        log.debug("REST request to delete Userx : {}", id);
        userxService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
