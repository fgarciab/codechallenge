/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fergarcia.codechallenge.web.rest.vm;
