package com.fergarcia.codechallenge.service;

import com.fergarcia.codechallenge.domain.Userx;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Userx.
 */
public interface UserxService {

    /**
     * Save a userx.
     *
     * @param userx the entity to save
     * @return the persisted entity
     */
    Userx save(Userx userx);

    /**
     * Get all the userxes.
     *
     * @return the list of entities
     */
    List<Userx> findAll();


    /**
     * Get the "id" userx.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Userx> findOne(Long id);

    /**
     * Delete the "id" userx.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
