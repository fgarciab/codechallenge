package com.fergarcia.codechallenge.service.impl;

import com.fergarcia.codechallenge.domain.enumeration.Status;
import com.fergarcia.codechallenge.service.UserxService;
import com.fergarcia.codechallenge.domain.Userx;
import com.fergarcia.codechallenge.repository.UserxRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Userx.
 */
@Service
@Transactional
public class UserxServiceImpl implements UserxService {

    private final Logger log = LoggerFactory.getLogger(UserxServiceImpl.class);

    private final UserxRepository userxRepository;

    public UserxServiceImpl(UserxRepository userxRepository) {
        this.userxRepository = userxRepository;
    }

    /**
     * Save a userx.
     *
     * @param userx the entity to save
     * @return the persisted entity
     */
    @Override
    public Userx save(Userx userx) {
        log.debug("Request to save Userx : {}", userx);
        return userxRepository.save(userx);
    }

    /**
     * Get all the userxes.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Userx> findAll() {
        return userxRepository.findAllByStatus(Status.ACTIVE);
    }


    /**
     * Get one userx by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Userx> findOne(Long id) {
        log.debug("Request to get Userx : {}", id);
        return userxRepository.findById(id);
    }

    /**
     * Delete the userx by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        Optional<Userx> userx = findOne(id);
        Userx u =userx.get();
        u.setStatus(Status.INACTIVE);
        save(u);
    }
}
