package com.fergarcia.codechallenge.repository;

import com.fergarcia.codechallenge.domain.Userx;
import com.fergarcia.codechallenge.domain.enumeration.Status;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Userx entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserxRepository extends JpaRepository<Userx, Long> {

    List<Userx> findAllByStatus(Status s);

}
