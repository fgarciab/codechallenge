package com.fergarcia.codechallenge.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    ACTIVE, INACTIVE
}
