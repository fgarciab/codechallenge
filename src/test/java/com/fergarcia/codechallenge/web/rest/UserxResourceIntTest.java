package com.fergarcia.codechallenge.web.rest;

import com.fergarcia.codechallenge.CodeChallengeApp;

import com.fergarcia.codechallenge.domain.Userx;
import com.fergarcia.codechallenge.repository.UserxRepository;
import com.fergarcia.codechallenge.service.UserxService;
import com.fergarcia.codechallenge.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.fergarcia.codechallenge.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fergarcia.codechallenge.domain.enumeration.Status;
/**
 * Test class for the UserxResource REST controller.
 *
 * @see UserxResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CodeChallengeApp.class)
public class UserxResourceIntTest {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_OF_BIRTH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OF_BIRTH = LocalDate.now(ZoneId.systemDefault());

    private static final Status DEFAULT_STATUS = Status.ACTIVE;
    private static final Status UPDATED_STATUS = Status.INACTIVE;

    @Autowired
    private UserxRepository userxRepository;

    @Autowired
    private UserxService userxService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserxMockMvc;

    private Userx userx;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserxResource userxResource = new UserxResource(userxService);
        this.restUserxMockMvc = MockMvcBuilders.standaloneSetup(userxResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Userx createEntity(EntityManager em) {
        Userx userx = new Userx()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .dateOfBirth(DEFAULT_DATE_OF_BIRTH)
            .status(DEFAULT_STATUS);
        return userx;
    }

    @Before
    public void initTest() {
        userx = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserx() throws Exception {
        int databaseSizeBeforeCreate = userxRepository.findAll().size();

        // Create the Userx
        restUserxMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userx)))
            .andExpect(status().isCreated());

        // Validate the Userx in the database
        List<Userx> userxList = userxRepository.findAll();
        assertThat(userxList).hasSize(databaseSizeBeforeCreate + 1);
        Userx testUserx = userxList.get(userxList.size() - 1);
        assertThat(testUserx.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testUserx.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testUserx.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testUserx.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createUserxWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userxRepository.findAll().size();

        // Create the Userx with an existing ID
        userx.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserxMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userx)))
            .andExpect(status().isBadRequest());

        // Validate the Userx in the database
        List<Userx> userxList = userxRepository.findAll();
        assertThat(userxList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserxes() throws Exception {
        // Initialize the database
        userxRepository.saveAndFlush(userx);

        // Get all the userxList
        restUserxMockMvc.perform(get("/api/users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userx.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @Test
    @Transactional
    public void getUserx() throws Exception {
        // Initialize the database
        userxRepository.saveAndFlush(userx);

        // Get the userx
        restUserxMockMvc.perform(get("/api/users/{id}", userx.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userx.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserx() throws Exception {
        // Get the userx
        restUserxMockMvc.perform(get("/api/users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserx() throws Exception {
        // Initialize the database
        userxService.save(userx);

        int databaseSizeBeforeUpdate = userxRepository.findAll().size();

        // Update the userx
        Userx updatedUserx = userxRepository.findById(userx.getId()).get();
        // Disconnect from session so that the updates on updatedUserx are not directly saved in db
        em.detach(updatedUserx);
        updatedUserx
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .status(UPDATED_STATUS);

        restUserxMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserx)))
            .andExpect(status().isOk());

        // Validate the Userx in the database
        List<Userx> userxList = userxRepository.findAll();
        assertThat(userxList).hasSize(databaseSizeBeforeUpdate);
        Userx testUserx = userxList.get(userxList.size() - 1);
        assertThat(testUserx.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testUserx.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testUserx.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testUserx.getStatus()).isEqualTo(UPDATED_STATUS);
    }



    @Test
    @Transactional
    public void updateNonExistingUserx() throws Exception {
        int databaseSizeBeforeUpdate = userxRepository.findAll().size();

        // Create the Userx

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserxMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userx)))
            .andExpect(status().isBadRequest());

        // Validate the Userx in the database
        List<Userx> userxList = userxRepository.findAll();
        assertThat(userxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserx() throws Exception {
        // Initialize the database
       // userxService.save(userx);

       // int databaseSizeBeforeDelete = userxRepository.findAll().size();

        updateUserx();

        // Get the userx
       // restUserxMockMvc.perform(put("/api/users/")
         //   .accept(TestUtil.APPLICATION_JSON_UTF8))
           // .andExpect(status().isOk());

        // Validate the database is empty
      //  List<Userx> userxList = userxRepository.findAll();
       // assertThat(userxList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Userx.class);
        Userx userx1 = new Userx();
        userx1.setId(1L);
        Userx userx2 = new Userx();
        userx2.setId(userx1.getId());
        assertThat(userx1).isEqualTo(userx2);
        userx2.setId(2L);
        assertThat(userx1).isNotEqualTo(userx2);
        userx1.setId(null);
        assertThat(userx1).isNotEqualTo(userx2);
    }
}
